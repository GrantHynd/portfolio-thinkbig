<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Must be authenticated */
Route::group(['middleware' => 'auth'], function () {
    
    /* Events */
    Route::get('/myevents/', 'EventController@myEvents');
    Route::get('/events/create', 'EventController@create');
    Route::post('/events', 'EventController@store');
    Route::get('/events/{event}/edit', 'EventController@edit');
    
    /* Resources */
    Route::get('/myresources', 'ResourceController@myResources');
    Route::get('/resources/create', 'ResourceController@create');
    Route::post('/resources', 'ResourceController@store');
    Route::get('/resources/{resource}/edit', 'ResourceController@edit');
    Route::patch('/resources/{resource}', 'ResourceController@update');
    Route::delete('/resources/{resource}', 'ResourceController@destroy');
});

/* Pages */
Route::get('/', 'PageController@home')->name('home');

/* Sessions */
Route::get('/login', 'SessionController@create')->name('login');
Route::post('/sessions/store', 'SessionController@store');
Route::get('/logout', 'SessionController@destroy');

/* Events */
Route::get('/events', 'EventController@index');

/* Resources */
Route::get('/resources', 'ResourceController@index');
Route::get('/resources/{resource}', 'ResourceController@show');

/* Admin only routes */
Route::group(['middleware' => 'admin', 'prefix' => 'administrator'], function () {
    Route::patch('/resources/{resource}/publish', 'ResourceController@publish');
    Route::patch('/resources/{resource}/unpublish', 'ResourceController@unpublish');

    Route::get('/categories', 'CategoryController@index');
    Route::post('/categories', 'CategoryController@store');
    Route::patch('/categories/{category}', 'CategoryController@update');
    Route::delete('/categories/{category}', 'CategoryController@destroy');
});

/* API calls */
Route::group(['prefix' => 'api'], function () { 
    Route::get('/tags', 'ApiController@getTags');
    Route::get('/events/{eventId?}/owner', 'ApiController@getEventOwnerDetails');
});
