
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
$(document).ready(function(){
    $(document).foundation();
});

window.Vue = require('vue');

/**
 * Required for fixing flatpickr display on IE 11
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#Polyfill
 */
if (typeof Object.assign != 'function') {
// Must be writable: true, enumerable: false, configurable: true
Object.defineProperty(Object, "assign", {
    value: function assign(target, varArgs) { // .length of function is 2
    'use strict';
    if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
    }

    var to = Object(target);

    for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
        for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
            to[nextKey] = nextSource[nextKey];
            }
        }
        }
    }
    return to;
    },
    writable: true,
    configurable: true
});
}

import EventList from './components/events/EventList.vue';
import EventCard from './components/events/EventCard.vue';
import EventCreateForm from './components/events/EventCreateForm.vue';
import EventEditForm from './components/events/EventEditForm.vue';
import TagInput from './components/TagInput.vue';

const app = new Vue({
    el: '#app',

    components: { EventList, EventCard, EventCreateForm, EventEditForm, TagInput }
});

