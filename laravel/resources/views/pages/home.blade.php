@extends('templates.front-page')
@section('content')
<div id="front">
	<div class="grid-container text-center">
		<p>If you require any assistance in creating an event or resource then please contact <a href="mailto:thinkbig@abertay.ac.uk">thinkbig@abertay.ac.uk</a>.
	</div>
	<div class="featured">
		<div class="featured__events">
			<a role="button" class="featured__button" href="{{ url('/events') }}">
				Events <br><br>
				<span class="fa fa-4x fa-calendar"></span><br><br>
				Find events created by and for the Abertay community, or submit your own. 
			</a>
		</div>
		<div class="featured__resources">
			<a role="button" class="featured__button" href="{{ url('/resources') }}">
				Resources <br><br>
				<span class="fa fa-4x fa-book"></span><br><br>
				Videos, documents, presentations and more. Find material that interests you!
			</a>
		</div>
	</div>

	<div class="introduction">
		<div class="purpose">
			<h1>The ThinkBIG Project</h1>
			<p>This website has been created as part of a wider project investigating ways to increase student understanding of business and enterprise.  This project is funded and supported by the Mathew Trust and Abertay University.  The website is designed to facilitate information sharing about events and resources related to business and enterprise.  Anyone from the University community can add resources and events, but these will be moderated by the ThinkBig@Abertay team.  We hope you find the site useful and will subscribe for notices of new events as they are added.</p>
			<p>For information about the wider project or any questions please email <a href="mailto:thinkbig@abertay.ac.uk">thinkbig@abertay.ac.uk</a></p>
			<hr>
		</div>
	</div>
</div>
@endsection


