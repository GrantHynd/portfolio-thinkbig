<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ThinkBIG</title>

    <script>var baseURL = '{{ env('APP_URL') }}';</script>
    <style>
        @font-face {
          font-family: 'FontAwesome';
          src: url('{{ url('/fonts/fontawesome-webfont.eot?674f50d287a8c48dc19ba404d20fe713') }}');
          src: url('{{ url('/fonts/fontawesome-webfont.eot?674f50d287a8c48dc19ba404d20fe713?#iefix&v=4.7.0') }}') format('embedded-opentype'), url('{{ url('/fonts/fontawesome-webfont.woff2?af7ae505a9eed503f8b8e6982036873e') }}') format('woff2'), url('{{ url('/fonts/fontawesome-webfont.woff?fee66e712a8a08eef5805a46892932ad') }}') format('woff'), url('{{ url('/fonts/fontawesome-webfont.ttf?b06871f281fee6b241d60582ae9369b9') }}') format('truetype'), url('{{ url('/fonts/fontawesome-webfont.svg?912ec66d7572ff821749319396470bde#fontawesomeregular') }}') format('svg');
          font-weight: normal;
          font-style: normal;
        }
    </style>
    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/css/app.min.css') }}" rel="stylesheet">
    
</head>
<body>
    <div id="app">
        @include('partials.nav')
        <div class="off-canvas-content" data-off-canvas-content>
            <!-- Your page content lives here -->
            @include('partials.banner')
            @yield('content')
        </div>
    </div>
    @include('partials.footer')
</body>
</html>