@extends('templates.default')
@section('content')
<div class="page">
    <div class="sidebar-left">
        <div class="sidebar-block">
            <div class="text-center">
                <a class="red-button" title="Create Resource" href="{{ url('/resources/create') }}"><span class="fa fa-plus"></span> Create Resource</a>
            </div>
        </div>
        @if(Auth::check())
            @if(Auth::user()->isAdmin())
                <div class="sidebar-block">
                    <div class="sidebar-block__title">
                        <span class="fa fa-filter"></span> Status
                    </div>
                    <div class="sidebar-block__body">
                        <ul class="sidebar-block__list">
                            <a class="{{ ($statusFilter == 'live') ? 'selected' : '' }}" href="{{ url('/resources?status=live&category='.$categoryFilter) }}"><li>Live</li></a>
                            <a class="{{ ($statusFilter == 'draft') ? 'selected' : '' }}" href="{{ url('/resources?status=draft&category='.$categoryFilter) }}"><li>Draft</li></a>
                        </ul>
                    </div>
                </div>
            @endif
        @endif
        <div class="sidebar-block">
            <div class="sidebar-block__title">
                <span class="fa fa-filter"></span> Categories
            </div>
            <div class="sidebar-block__body">
                <ul class="sidebar-block__list">
                    <a class="{{ ($categoryFilter == null) ? 'selected' : '' }}" href="{{ url('/resources?status='.$statusFilter) }}"><li>All</li></a>
                    @foreach($categoriesWithResources as $category)
                        <a class="{{ ($categoryFilter == $category->name) ? 'selected' : '' }}" href="{{ url('/resources?status='.$statusFilter.'&category='.$category->name) }}"><li>{{ $category->name }}</li></a>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="page-content">
        <h3 class="page-content__title">Resources</h3>
        <p class="page-content__sub-title">
            Filtered by <?php echo ($categoryFilter != null) ? $categoryFilter : 'All Categories' ?>
            <?php echo ($tagFilter != null) ? '  |  '.$tagFilter .' tag' : '' ?>
        </p>
        {{ $resources->links() }}
        <div class="resource-list align-top">
            @if($resources->total() == 0)
                <p>There are no {{ $statusFilter }} resources for this category, try changing your filtering options.
            @else 
                @foreach($resources as $resource)
                    @include ('partials.resource-card')
                @endforeach
            @endif
        </div>
        {{ $resources->links() }}
    </div>
</div>
@endsection