@extends('templates.default')
@section('content')
<div class="page">
    <div class="sidebar-left">
        <div class="sidebar-block">
            <div class="text-center">
                <a class="red-button" title="Create Resource" href="{{ url('/resources/create') }}"><span class="fa fa-plus"></span> Create Resource</a>
            </div>
        </div>
        <div class="sidebar-block">
            <div class="sidebar-block__title">
                <span class="fa fa-filter"></span> Status
            </div>
            <div class="sidebar-block__body">
                <ul class="sidebar-block__list">
                    <a class="{{ ($statusFilter == 'live') ? 'selected' : '' }}" href="{{ url('/myresources?status=live') }}"><li>Live</li></a>
                    <a class="{{ ($statusFilter == 'draft') ? 'selected' : '' }}" href="{{ url('/myresources?status=draft') }}"><li>Draft</li></a>
                </ul>
            </div>
        </div>
    </div>

    <div class="page-content">
        <h3 class="page-content__title">My Resources</h3>
        {{ $resources->links() }}
        <div class="resource-list align-top">
            @if($resources->total() == 0)
                <p>There are no {{ $statusFilter }} resources, try changing your filtering options.
            @else 
                @foreach($resources as $resource)
                    @include ('partials.resource-card')
                @endforeach
            @endif
        </div>
        {{ $resources->links() }}
    </div>
</div>
@endsection