@extends('templates.default')
@section('content')
<div class="page">
    <div class="sidebar-left">
        <div class="sidebar-block">
            <div class="text-center">
                <a class="red-button" title="Create Resource" href="{{ url('/resources/create') }}"><span class="fa fa-plus"></span> Create Resource</a>
            </div>
        </div>
        <div class="sidebar-block">
            <div class="sidebar-block__title">
                Related Resources
            </div>
            <div class="sidebar-block__body">
                <ul class="sidebar-block__list">
                    @if(count($relatedResources))
                        @foreach($relatedResources as $relatedResource)
                            <a href="{{ url('/resources/'.$relatedResource->id) }}"><li>{{ $relatedResource->name }}</li></a>
                        @endforeach
                    @else
                        <p>There are no related resources.</p>
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="resource">
            <div class="resource__image {{ $resource->linkType->name.'-link' }}">
                <img src="{{ url('/images/'.$resource->linkType->image_name) }}" alt="resource icon"/>
            </div>
            <h3 class="page-content__title">{{ $resource->name }}</h3>
            <div class="resource__date">
                <p>Added on {{ $resource->created_at->toFormattedDateString() }}</p>
            </div>
            <hr>
            <div class="resource__description">
                <pre>{{ $resource->desc }}</pre>
            </div>
            <div class="resource__link">
                <a href="{{ $resource->link }}">
                    <div class="link__icon {{ $resource->linkType->name.'-link' }}"><span class="fa fa-3x fa-link"></span></div>
                    <div class="link__url">{{ $resource->link }}</div>
                </a>
            </div>
            <div class="resource__footer">
                <div class="resource__category">
                    Category: <a class="tag" href="{{ url('/resources?category='.$resource->category->name) }}">{{ $resource->category->name }}</a>
                </div>
                @if(count($resource->tags()->get()) > 0)
                <div class="resource__tags">
                    <p>Tags:
                    @foreach($resource->tags()->get() as $tag)
                        <a class="tag" href="{{ url('/resources?tag='.$tag->name) }}">#{{ $tag->name }}</a>
                    @endforeach
                    </p>
                </div>
                @endif
            </div>

            @if(Auth::check())
                @if(Auth::user()->isAdmin())
                    <div>
                        <hr>
                        <h5 class="page-content__title">Creator Information</h5>
                        <p><strong>Contact Details:</strong> {{ $resource->owner->displayname }} - {{ $resource->owner->email }}</p>
                        <p><strong>School:</strong> {{ $resource->owner->school_code }}</p>
                    </div>
                @endif
            @endif
        </div>
    </div>
</div>
@endsection