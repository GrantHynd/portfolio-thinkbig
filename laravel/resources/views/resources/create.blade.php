@extends('templates.default')
@section('content')
<div class="page">
    <div class="page-content">
        <h3 lass="page-content__title">Resource Details</h3>
        <form id="resource-form" method="POST" action="{{ url('/resources') }}">
            {{ csrf_field() }}
            <div class="form__left">
                <div class="form__name">
                    <label>Resource Title <span class="required">*</span></label>
                    <input class="form__text" type="text" name="name" value="{{ old('name') }}" placeholder="Give it a short distinct name">
                </div>

                <div class="form__description">
                    <label>Resource Description</label>
                    <textarea class="form__textarea" name="desc">{{ old('desc') }}</textarea>
                </div>

                <div class="form__link">
                    <label>Resource link (format http://example.com)<span class="required">*</span></label>
                    <input class="form__text" type="text" name="link" value="{{ old('link') }}" placeholder="paste in your link">
                </div>
            </div>
            <div class="form__right">
                <div class="form__category">
                    <label>Resource Category<span class="required">*</span></label>
                    <select class="form__select" name="category">
                        <option value selected="selected">Select a category</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div> 
                <div class="form__category">
                    <label>Resource Format Type<span class="required">*</span></label>
                    <select class="form__select" name="link_type">
                        <option value selected="selected">Select a format</option>
                        @foreach($linkTypes as $linkType)
                            <option value="{{ $linkType->id }}">{{ $linkType->name }}</option>
                        @endforeach
                    </select>
                </div> 

                <div class="form__tags">
                    <label>Resource Tags</label>
                    <tag-input></tag-input>
                </div> 
            </div>
            <div class="form__submit">
                <button class="green-button" title="Submit Resource"><span class="fa fa-save"></span> Submit Resource</button>
            </div>
        </form>

        @if(count($errors))
            <ul class="callout alert">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        @endif
    </div>
</div>
@endsection


