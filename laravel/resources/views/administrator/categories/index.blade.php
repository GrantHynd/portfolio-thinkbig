@extends('templates.default')
@section('content')
<div class="page">
    <div class="page-content">
        <h3 class="page-content__title">Manange Categories</h3>
        <div class="categories-list">
            @foreach($categories as $category) 
                <div class="category">
                    <form class="category-form category-update" method="POST" action="{{ url('/administrator/categories/'.$category->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form__name">
                            <input class="form__text" type="text" name="name" value="{{ $category->name }}">
                        </div>
                        <div class="form__submit">
                            <button class="button">Update</button>
                        </div>
                    </form>
                    <form class="category-form category-delete" method="POST" action="{{ url('/administrator/categories/'.$category->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <div class="form__delete">
                            <button class="red-button"><span class="fa fa-trash"></span></button>
                        </div>
                    </form>
                </div>
                <hr>
            @endforeach
        </div>
        <div class="category">
            <form class="category-form category-new" method="POST" action="{{ url('/administrator/categories') }}">
                {{ csrf_field() }}
                <div class="form__name">
                    <input class="form__text" type="text" name="name" value="">
                </div>
                <div class="form__submit">
                    <button class="green-button">Add Category</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection