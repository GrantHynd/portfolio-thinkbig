@extends('templates.default')
@section('content')
<div class="page">
    <div class="sidebar-left">
        <div class="sidebar-block">
            <div class="text-center">
                <a class="red-button" title="Create Event" href="{{ url('/events/create') }}"><span class="fa fa-plus"></span> Create Event</a>
            </div>
        </div>
    </div>

    <div class="page-content">
        <h3 class="page-content__title">My Events</h3>
        <div class="event-list">
            @foreach($userEvents as $event)
                @if(Auth::check())
                    @if(Auth::user()->isAdmin())
                        <event-card class="poster-card"  :event-id="{{ $event->id }}" :config="{ admin: true }"></event-card>
                    @else
                        <event-card class="poster-card"  :event-id="{{ $event->id }}"  :config="{ admin: false }"></event-card>
                    @endif
                @else 
                    <event-card class="poster-card"  :event-id="{{ $event->id }}" :config="{ admin: false }"></event-card>
                @endif
            @endforeach
        </div>
    </div>
</div>
@endsection