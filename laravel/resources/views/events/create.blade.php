@extends('templates.default')
@section('content')
<div class="page">
    <div class="page-content">
        <h3 lass="page-content__title">Event Details</h3>
        <event-create-form></event-create-form>
    </div>
</div>
@endsection