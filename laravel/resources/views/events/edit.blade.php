@extends('templates.default')
@section('content')
<div class="page">
    <div class="page-content">
        <h3 lass="page-content__title">Event Details</h3>
        <event-edit-form :event-id="{{ $event->id }}"></event-edit-form>
    </div>
</div>
@endsection