@extends('templates.default')
@section('content')
<div class="page">
    <div class="sidebar-left">
        <div class="sidebar-block">
            <div class="text-center">
                <a class="red-button" title="Create Event" href="{{ url('/events/create') }}"><span class="fa fa-plus"></span> Create Event</a>
            </div>
        </div>
        @if(Auth::check())
            @if(Auth::user()->isAdmin())
                <div class="sidebar-block">
                    <div class="sidebar-block__title">
                        <span class="fa fa-filter"></span> Status
                    </div>
                    <div class="sidebar-block__body">
                        <ul class="sidebar-block__list">
                            <a class="{{ ($statusFilter == 'live') ? 'selected' : '' }}" href="{{ url('/events?status=live') }}"><li>Live</li></a>
                            <a class="{{ ($statusFilter == 'draft') ? 'selected' : '' }}" href="{{ url('/events?status=draft') }}"><li>Draft</li></a>
                            <a class="{{ ($statusFilter == 'ended') ? 'selected' : '' }}" href="{{ url('/events?status=ended') }}"><li>Ended</li></a>
                        </ul>
                    </div>
                </div>
            @endif
        @endif
    </div>

    <div class="page-content">
        <h3 class="page-content__title">Events</h3>
        @if(Auth::check())
            @if(Auth::user()->isAdmin())
                <event-list :status="'{{ $statusFilter }}'" :config="{ admin: true }"></event-list>
            @else
                <event-list :status="'{{ $statusFilter }}'" :config="{ admin: false }"></event-list>
            @endif
        @else 
            <event-list :status="'{{ $statusFilter }}'" :config="{ admin: false }"></event-list>
        @endif
    </div>
</div>
@endsection