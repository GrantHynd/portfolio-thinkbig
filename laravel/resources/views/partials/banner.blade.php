<div class="page-banner" style="background-image: url({{ url('/images/featured-bg.jpg') }})">
    <div class="page-banner__logo">
        <img alt="ThinkBIG logo" src="{{ url('/images/thinkbig-logo-large.png') }}">
    </div>
    <div class="page-banner__content hide-for-small-only">
        <p class="page-banner__tagline">A hub for events and resources</p>
        <div class="page-banner__newsletter">
            @include ('partials.newsletter-signup-form')
        </div>
    </div>
</div>