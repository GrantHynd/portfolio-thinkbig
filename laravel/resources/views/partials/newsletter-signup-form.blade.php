<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
    <p>
        <strong>Sign up to our newsletter and be notified of new events and resources.</strong><br/>
        Remember to confirm your email.
    </p>
    <form action="//abertay.us16.list-manage.com/subscribe/post?u=091d868b9cba806bfb0f9266c&amp;id=9611f64fa0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll"> 
            <div class="mc-field-group">
                <input type="email" value="<?php echo (Auth::check()) ? Auth::user()->email : ''?>" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="email address">
            </div>
            <div id="mc_embed_submit" class="clear">
                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="green-button">
            </div>
            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_091d868b9cba806bfb0f9266c_9611f64fa0" tabindex="-1" value=""></div>
        </div>
    </form>
</div>
<!--End mc_embed_signup-->