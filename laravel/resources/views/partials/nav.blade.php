<div class="page-banner__nav">
  <div class="top-bar">
    <div class="top-bar-left">
      <ul data-dropdown-menu="" class="dropdown menu" role="menubar" data-e="okpcnh-e">
        <li id="logo" role="menuitem">
          <a href="http://my.abertay.ac.uk" accesskey="1" title="Click to return to the intranet"><img src="https://intranet.abertay.ac.uk/media/myabertay-logo.svg" alt=""></a>
        </li>
        <li class="menu-text hide-for-small-only" role="menuitem">
          <a href="{{ url('/') }}">ThinkBIG</a>
        </li>
      </ul>
    </div>
    <div class="top-bar-right">
      <ul class="dropdown menu" data-dropdown-menu>
        @if(!Auth::check())
          <li class="has-submenu" data-toggle="offCanvas"><a href="#">Menu</a></li>
        @else
          <li class="has-submenu" data-toggle="offCanvas"><a href="#"><span class="fa fa-user"></span> Hello, {{ Auth::user()->given_name }}</a></li>
        @endif
      </ul>
    </div>
  </div>

  <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
    <ul class="menu accordion-menu vertical" data-accordion-menu>
      <li>
        <i class="fa fa-th-large" aria-hidden="true"></i>
        <a href="http://my.abertay.ac.uk">Return to Dashboard</a>
      </li>
      <li>
        <i class="fa fa-home" aria-hidden="true"></i>
        <a href="{{ url('/') }}">Home</a>
      </li>
      <li>
        <i class="fa fa-plus-square-o" aria-hidden="true"></i>
        <a href="#">Events</a>
        <ul class="vertical menu nested">
          <li>
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <a href="{{ url('/events/create') }}">Create Event</a>
          </li>
          <li>
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <a href="{{ url('/myevents') }}">My Events</a>
          </li>
          <li>
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <a href="{{ url('/events') }}">All Events</a>
          </li>
        </ul>
      </li>
      <li>
        <i class="fa fa-plus-square-o" aria-hidden="true"></i>
        <a href="#">Resources</a>
        <ul class="vertical menu nested">
          <li>
            <i class="fa fa-book" aria-hidden="true"></i>
            <a href="{{ url('/resources/create') }}">Create Resource</a>
          </li>
          <li>
            <i class="fa fa-book" aria-hidden="true"></i>
            <a href="{{ url('/myresources') }}">My Resources</a>
          </li>
          <li>
            <i class="fa fa-book" aria-hidden="true"></i>
            <a href="{{ url('/resources') }}">All Resources</a>
          </li>
        </ul>
      </li>
      @if(!Auth::check())
        <hr>
        <li>
          <i class="fa fa-sign-in" aria-hidden="true"></i>
          <a href="{{ url('/login') }}">Login</a>
        </li>
      @endif
      @if (Auth::check()) 
        @if(Auth::user()->isAdmin())
          <li>
            <i class="fa fa-file-o" aria-hidden="true"></i>
            <a href="{{ url('/administrator/categories') }}">Manage Categories</a>
          </li>
        @endif
        <li>
          <i class="fa fa-sign-out" aria-hidden="true"></i>
          <a href="{{ url('/logout') }}">Logout</a>
        </li>
      @endif
    </ul>
  </div>
</div>