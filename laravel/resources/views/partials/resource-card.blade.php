 <div class="poster-card">
    <a href="{{ url('/resources/'.$resource->id) }}">
        <div class="poster-card__header">
            <div class="poster-card__image">
                <img src="{{ url('/images/'.$resource->linkType->image_name) }}"/>
            </div>
            <span class="poster-card__type">{{ $resource->linkType->name }}</span> <!-- change class to link-type -->
        </div>
        <div class="poster-card__body">
            <time class="poster-card__date">Added on {{ $resource->created_at->toFormattedDateString() }}</time>
            <div class="poster-card__title">{{ $resource->name }}</div>
            @if(Auth::check())
                @if(Auth::user()->isAdmin())
                    <div class="poster-card__owner">{{ $resource->owner->email }}</div>
                @endif
            @endif
            <div class="poster-card__category">{{ $resource->category->name }}</div>
        </div>
    </a>
    <div class="poster-card__footer">
        <div class="poster-card__tags">
            @foreach($resource->tags()->get() as $tag)
                <a class="tag" href="{{ url('/resources?status='.$statusFilter.'&tag='.$tag->name) }}">#{{ $tag->name }}</a>
            @endforeach
        </div>
        @if($statusFilter == 'live')
            <div class="poster-card__actions">
                <a href="http://www.facebook.com/sharer.php?u={{ url('/resources/'.$resource->id) }}" target="_blank"><span class="fa fa-facebook"></span></a>
            </div>
        @endif
    </div>
    @if(Auth::check())
        @if(Auth::user()->id == $resource->owner->id || Auth::user()->isAdmin())
            <div class="poster-card__footer">
                <div class="poster-card__actions">
                    @if(Auth::user()->isAdmin())
                        @if($resource->isLive())
                            <form class="button-form" method="POST" action="{{ url('administrator/resources/'.$resource->id.'/unpublish') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PATCH">
                                <button class="unpublish">Unpublish</button>
                            </form>
                        @else
                            <form class="button-form" method="POST" action="{{ url('administrator/resources/'.$resource->id.'/publish') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PATCH">
                                <button class="publish">Publish</button>
                            </form>
                        @endif
                    @endif
                    <a href="{{ url('/resources/'.$resource->id.'/edit') }}"><span class="fa fa-pencil"></span></a>
                    <form class="button-form" method="POST" action="{{ url('/resources/'.$resource->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <button><span class="fa fa-trash"></span></button>
                    </form>
                </div>
            </div>
        @endif
    @endif
</div>