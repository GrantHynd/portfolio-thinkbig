<div class="footer">
    <div class="page-banner__content show-for-small-only">
        <div class="page-banner__newsletter">
            @include ('partials.newsletter-signup-form')
        </div>
    </div>
    <div class="grid-container">
        <p>This project is funded by Matthew Trust and ATLEF 5.</p>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='BIRTHDAY';ftypes[3]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<script src="{{ url('/js/jquery.min.js') }}"></script>
<script src="{{ url('/js/foundation.min.js') }}"></script>
<script src="{{ url('/js/app.min.js') }}"></script>