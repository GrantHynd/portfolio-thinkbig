<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Event;
use Auth;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getTags()
    {
        return Tag::latest()->get();
    }

    public function getEventOwnerDetails($eventId = null)
    {
        $ownerDetails['email'] = 'No email found';
        $ownerDetails['currentUserisOwner'] = false;
        
        $event = Event::find($eventId);
        if($event != null) {
            $ownerDetails = $event->owner()->first();

            $ownerDetails['email'] = $ownerDetails->email;
            if(Auth::user()->id == $ownerDetails->id) {
                $ownerDetails['currentUserisOwner'] = true;
            } else {
                $ownerDetails['currentUserisOwner'] = false;
            }
        }
        return $ownerDetails;
    }
}