<?php

namespace App\Http\Controllers;

use App\Resource;
use App\Category;
use App\LinkType;
use App\Tag;
use App\Http\Requests\ResourceRequest;
use Illuminate\Http\Request;
use Auth;

class ResourceController extends Controller
{
    public function index(Request $request) {   
        $tagFilter = $request['tag'];
        $categoryFilter = $request['category'];
        $statusFilter = ($request['status'] == null) ? 'live' : $request['status'];

        $categoriesWithResources = $this->getCategoriesThatHaveResources($statusFilter);

        $resources = Resource::latest();
        if($tagFilter != null) {
            $resources = $this->applyTagFilter($tagFilter);
        }
        else if($categoryFilter != null) {
            $resources = $this->applyCategoryFilter($categoryFilter);
        } 
        $resources = $this->applyStatusFilter($statusFilter, $resources);
        $resources = $resources->paginate(6);
        $resources->appends(request()->query());
        return view('resources.index', compact('resources', 'categoriesWithResources', 'statusFilter', 'categoryFilter', 'tagFilter'));
    }

    private function applyTagFilter($tagFilter) {
        $tag = Tag::where('name', $tagFilter)->first();
        $resources = $tag->resources()->wherePivot('tag_id', '=', $tag->id);
        return $resources;
    }

    private function applyCategoryFilter($categoryFilter) {
        $category = Category::where('name', $categoryFilter)->first();
        $resources = $category->resources();
        return $resources;
    }

    private function applyStatusFilter($statusFilter, $resources = null) {
        if($resources == null) {
            $resources = Resource::has('category');
        }

        if($statusFilter == 'draft') {
            $resources = $resources->draft();
        } 
        else if($statusFilter == 'live') {
            $resources = $resources->live();
        }
        return $resources;
    }

    private function getCategoriesThatHaveResources($statusFilter) {
        $categoryIds = $this->applyStatusFilter($statusFilter)->pluck('category_id');
        $categoriesWithResources = Category::all()->whereIn('id', $categoryIds);
        return $categoriesWithResources;
    }

    public function myResources(Request $request) {
        $statusFilter = ($request['status'] == null) ? 'live' : $request['status'];

        if($statusFilter == 'draft') {
            $resources = Resource::latest()->ownedByCurrentUser()->draft()->paginate(6);
        }
        else if($statusFilter == 'live') {
            $resources = Resource::latest()->ownedByCurrentUser()->live()->paginate(6);
        }
        $resources->appends(request()->query());
        return view('resources.myresources', compact('resources', 'statusFilter'));
    }

    public function create()
    {
        $categories = Category::all()->sortBy('name');
        $linkTypes = LinkType::all()->sortBy('name');
        return view('resources.create', compact('categories', 'linkTypes'));
    }

    public function store(ResourceRequest $request)
    {
        $resource = Resource::create([
            'user_id' => $request->user()->id,
            'name' => $request['name'],
            'desc' => $request['desc'],
            'link' => $request['link'],
            'status' => 'draft',
            'category_id' => $request['category'],
            'link_type_id' => $request['link_type']
        ]);

        foreach($request['tags'] as $selectedTag) {
            $tag = Tag::firstOrNew(['name' => $selectedTag]);
            $tag->save();
            $resource->tags()->attach($tag);
        }

        return redirect(url('/resources/'.$resource->id));
    }

    public function show(Resource $resource)
    {
        $relatedResources = Resource::latest()->where('category_id', '=', $resource->category_id)->where('id', '!=', $resource->id)->limit(5)->get();
        return view('resources.show', compact('resource', 'relatedResources'));
    }

    public function edit(Resource $resource)
    {
        $categories = Category::all()->sortBy('name');
        $linkTypes = LinkType::all()->sortBy('name');

        $selectedTags = [];
        foreach($resource->tags()->get(['id', 'name']) as $tag) {
            $formattedTag = json_encode(array('id' => $tag->id, 'name' => $tag->name));
            array_push($selectedTags, $formattedTag);
        }
        return view('resources.edit', compact('resource', 'categories', 'linkTypes', 'selectedTags'));
    }

    public function update(ResourceRequest $request, Resource $resource)
    {
        $resource->update([
            'name' => $request['name'],
            'desc' => $request['desc'],
            'link' => $request['link'],
            'category_id' => $request['category'],
            'link_type_id' => $request['link_type']
        ]);

        $selectedTagIds = [];
        foreach($request['tags'] as $tagName) {
            $tag = Tag::firstOrNew(['name' => $tagName]);
            $tag->save();
            array_push($selectedTagIds, $tag->id);
        }
        $resource->tags()->sync($selectedTagIds);
        return redirect(url('/resources/'.$resource->id));
    }

    public function publish(Resource $resource)
    {
        $resource->update([
            'status' => 'live'
        ]);
        return redirect(url('/resources/'.$resource->id));
    }

    public function unpublish(Resource $resource)
    {
        $resource->update([
            'status' => 'draft'
        ]);
        return redirect(url('/resources/'.$resource->id));
    }

    public function destroy(Resource $resource)
    {
        $resource->tags()->wherePivot('resource_id', '=', $resource->id)->detach();
        $resource->delete();
        return redirect(url('/resources'));
    }
}
