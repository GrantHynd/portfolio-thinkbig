<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

include(app_path().'/includes/oauth.php'); // Test user data

class SessionController extends Controller
{
    /**
     * Show the login page.
     *
     * @return Illuminate\View\View
     */
    public function create() {
        return view('sessions.create');
    }

    /**
     * Use the session variable to find the user, else create new user.
     * Then login user and redirect them to their intended destination.
     * 
     * @return Illuminate\Contracts\Routing\ResponseFactory
     */
    public function store() {
        /**
         * Sensitive Information: Redacted for security purposes
         * 
         */

        $user->save();
        auth()->login($user);
        return redirect()->intended(url('/'));
    }

    /**
     * Logout user and wipe session data.
     * 
     * Note: May need to inform Office 365 of logout.
     *
     * @return Illuminate\Contracts\Routing\ResponseFactory
     */
    public function destroy() {
        auth()->logout();

        Session::flush();
        session_unset();
        session_destroy();
        session_write_close();

        return redirect()->home(); 
    }
}
