<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('administrator.categories.index', compact('categories'));
    }

    public function store(Request $request)
    {
        Category::create([
            'name' => $request['name']
        ]);
        return redirect(url('/administrator/categories'));
    }

    public function update(Request $request, Category $category)
    {
        $category->update([
            'name' => $request['name']
        ]);
        return redirect(url('/administrator/categories'));
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(url('/administrator/categories'));
    }
}
