<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventRequest;
use App\Event;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index(Request $request) {
        $statusFilter = ($request['status'] == null) ? 'live' : $request['status'];
        return view('events.index', compact('statusFilter'));
    }

    public function myEvents() {
        $userEvents = Event::latest()->where('user_id', '=', Auth::user()->id)->get();
        return view('events.myevents', compact('userEvents'));
    }

    public function create() {
        return view('events.create');
    }

    public function store(EventRequest $request) {
        Event::create([
            'id' => $request['event']['id'],
            'user_id' => $request->user()->id
        ]);
        return redirect(url('/myevents'));
    }

    public function edit(Event $event) {
        return view('events.edit', compact('event'));
    }

    public function destroy(Event $event) {
        $event->delete();
        return redirect(url('/myevents'));
    }
}
