<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class Resource extends Model
{
    protected $fillable = ['user_id', 'name', 'desc', 'link', 'status', 'category_id', 'link_type_id'];
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get format for dates.
     * 
     * Note: .u is required for live server but it produces a error on local server. 
     * 
     * TODO Find solution that works on both.
     *
     * @return string
     */
    public function getDateFormat() {
        return 'Y-m-d H:i:s.u';
    }

    /**
     * Get the DateTime in the format for MS SQL.
     *
     * @return string
     */
    public function fromDateTime($value) {
        return substr(parent::fromDateTime($value), 0, -3);
    }

    public function isLive() {
        $isLive = false;
        if($this->status == 'live') {
            $isLive = true;
        }
        return $isLive;
    }

    public function scopeOwnedByCurrentUser($query) {
        return $query->where('user_id', '=', Auth::user()->id);
    }

    public function scopeDraft($query) {
        return $query->where('status', '=', 'draft');
    }

    public function scopeLive($query) {
        return $query->where('status', '=', 'live');
    }

    public function owner() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function linkType() {
        return $this->belongsTo(LinkType::class);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
}
