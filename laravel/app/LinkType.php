<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkType extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    public function resources() {
        return $this->hasMany(Resource::class);
    }
}
