<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;
    protected $fillable = [
        /**
         * Sensitive Information: Redacted for security purposes
         * 
         */
    ];

    public function isAdmin(){
        $isAdmin = false;
        if($this->permission_type == 'admin') {
            $isAdmin = true;
        }
        return $isAdmin;
    }

    public function events() {
        return $this->hasMany(Event::class);
    }
}
