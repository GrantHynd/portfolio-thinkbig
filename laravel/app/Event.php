<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    protected $fillable = ['id', 'user_id'];
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get format for dates.
     * 
     * Note: .u is required for live server but it produces a error on local server. 
     * 
     * TODO Find solution that works on both.
     *
     * @return string
     */
    public function getDateFormat() {
        return 'Y-m-d H:i:s.u';
    }

    /**
     * Get the DateTime in the format for MS SQL.
     *
     * @return string
     */
    public function fromDateTime($value) {
        return substr(parent::fromDateTime($value), 0, -3);
    }

    public function owner() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
