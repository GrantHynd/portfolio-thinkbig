<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
/**
 * Sensitive Information: Redacted for security purposes
 * 
 */
});

$factory->define(App\Resource::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'name' => $faker->sentence,
        'desc' => $faker->paragraph,
        'link' => 'http://abertaysurvey.app/',
        'status' => 'draft',
        'category_id' => 1,
        'link_type_id' => 1
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique->word,
    ];
});

$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique->word,
    ];
});

$factory->define(App\LinkType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique->word,
        'image_name' => $faker->word .'.png'
    ];
});