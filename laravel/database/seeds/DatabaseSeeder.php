<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\QuestionType::class)->create(['type' => 'dropdown']);
        factory(App\QuestionType::class)->create(['type' => '7 point scale']);
        factory(App\QuestionType::class)->create(['type' => 'multiselect']);

        /**
         * Sensitive Information: Redacted for security purposes
         * 
         */

        if( $user = App\User::all()->where('role', 'Staff')->first() ) 
        {
            factory(App\Survey::class, 10)->create(['user_id' => $user->id])
            ->each(function($survey) 
            {
                factory(App\Section::class, 2)->create(['survey_id' => $survey->id])
                ->each(function($section) 
                {
                    factory(App\Question::class, 5)->create(['section_id' => $section->id, 'type_id' => 1])
                    ->each(function($question) 
                    {
                        factory(App\QuestionOption::class, 4)->create(['question_id' => $question->id]);
                    });

                    factory(App\Question::class, 5)->create(['section_id' => $section->id, 'type_id' => 2]);
                });

                factory(App\Instance::class, 8)->create(['user_id' => $survey->user_id, 'survey_id' => $survey->id])
                ->each(function($instance)
                {
                    $questionIds = $instance->survey->sections()->first()->questions()->pluck('id')->toArray();
                    $userIds = App\User::all()->pluck('id')->toArray();
                    $syncData = [];
                    for($i = 0; $i < count($questionIds); $i++) { 
                        $syncData[$questionIds[$i]] = ['user_id' => $userIds[$i], 'answer' => rand(1, 4)];     
                    }

                    $instance->questions()->sync($syncData);
                    
                });
            });
        }
    }
}
