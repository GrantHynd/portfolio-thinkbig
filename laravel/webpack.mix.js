let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath(path.normalize('../www/subfolder'))
    .sass('resources/assets/sass/app.scss', '../www/subfolder/css/app.min.css')
    .js('resources/assets/js/app.js', '../www/subfolder/js/app.min.js')
    .copy('vendor/components/jquery/jquery.min.js', '../www/subfolder/js')
    .copy('vendor/zurb/foundation/dist/js/foundation.min.js', '../www/subfolder/js')
    .copy('node_modules/font-awesome/fonts','../www/subfolder/fonts');
