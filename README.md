# ThinkBIG Events and Resources Hub
Version: 1.0

## Application Summary ##
A web application which aims to act as a sharing platform for education events and resources. Staff and students can submit events and resources which are stored as drafts until approved by admins.

## Features ##	
### Accounts ###

- Users sign-in to the application using their university account (no account creation required).


### Events ###

- Users can submit draft versions of Eventbrite events via the website (utilises Eventbrite API).
- Admins can filter events by their published status.
- Admins can edit any event.
- Admins can publish / unpublish submitted events.
- Event owners can filter their own events by publish status.
- Event owners can edit their own events.


### Resources ###

- Users can submit resources (a link with a description and tags).
- Users can filter all resources by category or tag.
- Admins can filter resources by category, tag or published status.
- Admins can edit any resource.
- Admins can publish / unpublish submitted resources.
- Admins can add, update and delete categories.
- Resource owners can filter their own resources by publish status.
- Resource owners can edit their own resources.


### Misc ###

- Users can sign-up to a Mailchimp newsletter.
- Responsive and cross browser compatible  design so that the web application is usable across major devices and browsers. 

## Who do I talk to? ##
Software Developer - Grant Hynd grant.hynd@gmail.com
